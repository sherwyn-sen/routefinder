﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Routefinder
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException()
        {

        }

        public InvalidInputException(string message) : base(message)
        {

        }

        public InvalidInputException(string message, Exception inner)
        {

        }
    }

    public class SelectionDoesNotExistException : Exception
    {
        public SelectionDoesNotExistException()
        {

        }

        public SelectionDoesNotExistException(string message) : base(message)
        {

        }

        public SelectionDoesNotExistException(string message, Exception inner)
        {

        }
    }

    public class NoAirportMatchedException : Exception
    {
        public NoAirportMatchedException()
        {

        }

        public NoAirportMatchedException(string message) : base(message)
        {

        }

        public NoAirportMatchedException(string message, Exception inner)
        {

        }
    }
}

﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace Routefinder
{
    public class FullRoute
    {
        // constants
        private const int Noparents = -1;
        
        // private members
        private Airport _departure, _arrival;
        private LinkedList<Airport> _connections;
        private RouteDatabase _routeDatabase;
        private bool _routeFound;
        
        
        // constructors
        public FullRoute(Airport departure, Airport arrival, RouteDatabase routeDatabase)
        {
            // initialize from arguments
            _departure = departure;
            _arrival = arrival;
            _routeDatabase = routeDatabase;
            
            // other members
            _connections = new LinkedList<Airport>();
            _routeFound = false;

            GetRoute();
        }
        
        // get the route via breadth-first search
        private void GetRoute()
        {
            var flights = new Queue<int>(); // queue for storing the children nodes
            var parents = new Dictionary<int, int>(); // store the parents of each child accessed in a dictionary so we can trace the route later
            var visited = new HashSet<int>(); // keep track of which airports have already been visited
            
            // load the departure into the queue and provide an entry for it in the parents dictionary
            flights.Enqueue(_departure.Id);
            parents[_departure.Id] = Noparents;
            visited.Add(_departure.Id);
 
            // look up all the possible routes from the departure
            while(flights.Any())
            {
                // the current parent is the top of the queue
                int currentParent = flights.Peek();
                

                
                // add each of the destinations to the queue and store what their parent flight
                try 
                {
                    foreach (int destinationId in _routeDatabase.Routes[currentParent])
                    {
                        // only advance if this id has not been visited yet
                        if (visited.Contains(destinationId)) continue;
                        
                        // queue up the current destination
                        flights.Enqueue(destinationId);
                        visited.Add(destinationId);
                            
                        // store the parent of this destination (child)
                        parents[destinationId] = currentParent;
                            
                        // check if we have "arrived" at the arrival airport
                        if (destinationId == _arrival.Id) 
                        {
                            _routeFound = true;
                            break;
                        }

                    }
                }
                // ignore any "airports without destinations" (lol)
                catch (KeyNotFoundException)
                {}
                
                // pop off the top item in the queue so we can advance to the next parent
                flights.Dequeue();
            }
            
            // we traversed all of the possible routes and no routes to the arrival airport were found
            if (!_routeFound)
                return;

            // use the parents Dictionary to trace the route
            Console.WriteLine("\nRoute found!");
            TraceRoute(parents);
        }
        
        private static void PrintParents(IDictionary<int, int> parents)
        {
            string[] lines = (from line in parents select $"c: {line.Key} p: {line.Value}").ToArray();
            Console.WriteLine(string.Join(Environment.NewLine, lines));
        }
        
        // trace the route from the departure to the arrival from the parents list, initializing Airport objects for each one
        private void TraceRoute(IDictionary<int, int> parents)
        {
            int currentLayoverId = _arrival.Id;            

            do
            {
                // get the airport info for the current layover from the id
                Airport currentLayover = _routeDatabase.Airports.First(airport => airport.Value.Id == currentLayoverId).Value;

                // add the current layover to the beginning of the list (we're going backwards from arrival to departure) 
                _connections.AddFirst(currentLayover);
                currentLayoverId = parents[currentLayoverId];
                
            } while (currentLayoverId != Noparents); // loop until we reach the destination, which has no parent

        }
        
        // public methods
        public override string ToString()
        {
            string connectionsString = "No possible routes.";
            
            // show the departure and arrival names in the header
            string header = $@"
            Route from {_departure.FullCity} to {_arrival.FullCity}
            -------------------------
            ";

            // get the names for all of the connections and make an arrow separated list
            if (_connections.Any())
            {
                string[] connectionNames = (from connection in _connections select " -> " + connection.Selection).ToArray();
                connectionsString = string.Join("\n            ", connectionNames);
            }
            
            return header + connectionsString + "\n";
        }
    }
}
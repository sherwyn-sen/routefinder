﻿using System;
using System.Diagnostics;

namespace Routefinder
{
    public static class Utils 
    {
        public static void StopStopwatch(Stopwatch stopwatch)
        {
            stopwatch.Stop();
            Console.Write(stopwatch.ElapsedTicks + "\t");
            stopwatch.Reset();
        }
    }
}
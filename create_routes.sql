CREATE TABLE airports (
      "id" INTEGER
    , "name" TEXT
    , "city" TEXT
    , "country" TEXT
    , "iata" TEXT
    , "icao" TEXT
    , "latitude" REAL
    , "longitude" REAL
    , "altitude" INTEGER
    , "timezone" REAL
    , "dst" TEXT
    , "tz_info" TEXT
    , "type" TEXT
    , "source" TEXT
);

CREATE TABLE routes (
      "airline" TEXT
    , "airline_id" INTEGER
    , "source_airport" TEXT
    , "source_airport_id" INTEGER
    , "destination_airport" TEXT
    , "destination_airport_id" INTEGER
    , "codeshare" TEXT
    , "stops" INTEGER
    , "equipment" TEXT
)
﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Routefinder
{
    public class RouteDatabase
    {
        private DatabaseConnection _dc;
        public SortedList<string, Airport> Airports { get; private set; }
        public Dictionary<int, HashSet<int>> Routes { get; private set; }

        
        public RouteDatabase(string databaseFile)
        {
            _dc = new DatabaseConnection(databaseFile);
            Airports = new SortedList<string, Airport>();
            Routes = new Dictionary<int, HashSet<int>>();
            GetAirports();
            GetRoutes();
        }

        public int CountItems(string tableName, string fieldName)
        {
            string totalItemsQuery = $"SELECT COUNT({fieldName}) FROM {tableName}";
            var command = new SQLiteCommand(totalItemsQuery, _dc.Con);
            SQLiteDataReader reader = command.ExecuteReader();
            reader.Read();
            int totalItems = Convert.ToInt32(reader[$"COUNT({fieldName})"]);
            return totalItems;
        }

        private Airport ReadAirport(SQLiteDataReader reader)
        {

            string name = (string) reader["name"];
            string city = (string) reader["city"];
            string country = (string) reader["country"];
            var airport = new Airport
            {
                Id = Convert.ToInt32(reader["id"]),
                Name = name,
                City    = city, 
                Country = country
            };
            return airport;
        }

        public void GetAirports()
        {
            Console.WriteLine("Loading the airports...");

            // get the total amount of airports in the database
            int totalAirports = CountItems("airports", "id");
            int airportsRead = 1;

            // get the actual airports
            // query the database for the needed fields
            string airportQuery = "SELECT id, name, city, country FROM airports ORDER BY name ASC";
            var command = new SQLiteCommand(airportQuery, _dc.Con);
        
            // read the resulting entries into the Airports list
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (airportsRead % 27 == 0 || airportsRead >= totalAirports)
                    Console.Write("Airports Loaded: {0} / {1}\r", airportsRead, totalAirports);
                Airport airport = ReadAirport(reader);
      
                
                // add these three keys for the airport so we can easily fuzzy search by any of the three categories
                try {
                    Airports.Add(airport.Name, airport);
                    Airports.Add(airport.City, airport);
                    Airports.Add(airport.Country, airport);
                }
                catch (ArgumentException) {} // just don't add the duplicate

                ++airportsRead;
            }

            Console.WriteLine();
        }

        public void GetRoutes()
        {
            Console.WriteLine("Loading the routes...");

            int totalRoutes = CountItems("routes", "source_airport_id");
            int routesRead = 1;
        
            // query the database for the needed route fields
            string routeQuery = "SELECT source_airport_id, destination_airport_id FROM routes ORDER BY source_airport_id";
            var command = new SQLiteCommand(routeQuery, _dc.Con);

            // read the resulting entries into Routes
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Console.Write("Routes Loaded: {0} / {1}\r", routesRead, totalRoutes);
                int sourceId = Convert.ToInt32(reader["source_airport_id"]);
                int destinationId = Convert.ToInt32(reader["destination_airport_id"]);
                if (!Routes.ContainsKey(sourceId))
                {
                    Routes[sourceId] = new HashSet<int>();
                }

                Routes[sourceId].Add(destinationId);
                ++routesRead;
            }

            Console.WriteLine();
        }
        
        // search for possible airports from the database (fuzzy search)
        public IList<Airport> SearchAirport(string input, int maxMatches)
        {
            // fuzzy search definition
            string likeMatch = input + "%";
            
            // query possibly matching airports from the database
            string sqlString = string.Format(@"
            SELECT id, name, city, country FROM airports 
                WHERE name LIKE ?
                OR city LIKE ?
                OR country LIKE ?
            ORDER BY city ASC
            LIMIT {0}
            ", maxMatches.ToString());
            var command = new SQLiteCommand(sqlString, _dc.Con);
            command.Parameters.Add(new SQLiteParameter("name", likeMatch));
            command.Parameters.Add(new SQLiteParameter("city", likeMatch));
            command.Parameters.Add(new SQLiteParameter("country", likeMatch));
            SQLiteDataReader reader = command.ExecuteReader();
            
            // read all the data from the reader into a list and return it
            var matchedAirports = new List<Airport>();
            while (reader.Read())
            {
                matchedAirports.Add(new Airport
                {
                    Id = Convert.ToInt32(reader["id"]), 
                    Name = (string)reader["name"],
                    City = (string)reader["city"],
                    Country = (string)reader["country"]
                });
            }

            return matchedAirports;
        }
    }
}
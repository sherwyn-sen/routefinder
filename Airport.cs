using System;

namespace Routefinder
{
    public class Airport
    {
        // private members 
        private int _id;
        private string _name, _city, _country;
        
        // properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string FullCity => $"{City}, {Country}";
        public string Selection => $"{City}, {Country} - {Name}";
    }
} 

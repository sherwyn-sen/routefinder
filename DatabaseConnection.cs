﻿using System;
using System.Data.SQLite;

namespace Routefinder
{
    public class DatabaseConnection
    {
        public SQLiteConnection Con;
        public string DatabaseFile;

        public DatabaseConnection(string databaseFile)
        {
            DatabaseFile = databaseFile;
            string conString = "Data Source=" + DatabaseFile + ";Version=3;";
            Con = new SQLiteConnection(conString);
            Con.Open(); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Routefinder
{
    internal class ConsoleRoutefinder : Routefinder
    {
        public const int MaxMatches = 40;

        private static void Main(string[] args)
        {
            var consoleRoutefinder = new ConsoleRoutefinder();
        }
    
        private ConsoleRoutefinder()
        {

        }
    
        protected override void StartUserInterface()
        {
            Console.WriteLine("Welcome to the Flight Routefinder!");
            while (true)
            {
                Console.WriteLine("Follow the prompts or type \"qquit\" to quit at any time.");
                Console.WriteLine("==============================================");

                // get the departure and arrival airports
                Console.WriteLine("Departure Airport");
                Console.WriteLine("----------------------------");
                Airport departureAirport = InputAirportSearch();
                Console.WriteLine("\nArrival Airport");
                Console.WriteLine("----------------------------");
                Airport arrivalAirport = InputAirportSearch();

                // find and show routes between cities
                Console.WriteLine("Searching for a possible route...");
                var fullRoute = new FullRoute(departureAirport, arrivalAirport, _routeDatabase);
                Console.WriteLine(fullRoute);

            }
        }

        private string GetInput()
        {
            var input = Console.ReadLine();
            if (input == "qquit")
            {
                Environment.Exit(0);
            }

            return input;
        }

        private Airport InputAirportSearch()
        {
            // user inputs an airport
            Console.WriteLine("Please search for an Airport: ");
            string input = GetInput();
            return ProcessInputAirportSearch(input);
        }

        private Airport ProcessInputAirportSearch(string input)
        {

            Console.WriteLine("Searching for Matches...");
            var matches = _routeDatabase.SearchAirport(input, MaxMatches);
            if (matches.Any())
                return InputAirportSelection(matches);

            // no matches found
            Console.WriteLine("ERROR: No airports found with that search string. Please try again.");
            return InputAirportSearch();
        }

        // given a list of matches, allow the user to select which airport they want based on its id
        private Airport InputAirportSelection(IList<Airport> matches)
        {
            string input = null;
            while (true)
            {
                try
                {
                    Console.WriteLine("Please select an airport from below or search for another airport: ");
                    // print the selection list
                    for (int i = 0; i < matches.Count(); ++i)
                    {
                        Console.WriteLine("{0}. {1}", i, matches[i].Selection);
                    }

                    input = GetInput();

                    // check if the input is valid
                    int selection = int.Parse(input);

                    // check if the selection is in the selection list
                    if (selection >= 0 && selection < matches.Count())
                        return matches[selection];

                    // the user inputted an invalid selection
                    Console.WriteLine("ERROR: This selection does not exist.");
                }

                // if the input is nonstandard
                catch (Exception e)
                {
                    if (e is ArgumentNullException || e is FormatException || e is OverflowException)
                    {
                        // if the input is unreadable, let the user input an airport again otherwise treat it like it's a new airport search
                        return input == null ? InputAirportSearch() : ProcessInputAirportSearch(input);
                    }
                    throw;
                }
            }
        }
        

    }
}

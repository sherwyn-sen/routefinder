﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Routefinder
{
    internal abstract class Routefinder
    {
    
        protected RouteDatabase _routeDatabase;

        protected Routefinder()
        {
            LoadFromDatabase();
            StartUserInterface();
        }
        private void LoadFromDatabase()
        {
            _routeDatabase = new RouteDatabase(
                databaseFile: "'C:\\Users\\SHS0618\\RiderProjects\\tutorial\\Routefinder\\routefinder.sqlite'"
            );
        }

        protected abstract void StartUserInterface();
    }
}
